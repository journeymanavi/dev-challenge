/**
 * This javascript file will constitute the entry point of your solution.
 *
 * Edit it as you need.  It currently contains things that you might find helpful to get started.
 */

// This is not really required, but means that changes to index.html will cause a reload.
require('./site/index.html')
// Apply the styles in style.css to the page.
require('./site/style.css')

// if you want to use es6, you can do something like
//     require('./es6/myEs6code')
// here to load the myEs6code.js file, and it will be automatically transpiled.

// Change this to get detailed logging from the stomp library
global.DEBUG = false

// Solution
const StompClient = require('./es6/StompClient.js');
const FXPriceReceivedAction = require('./es6/actions/FXPriceReceivedAction.js');
const SortedPricesTable = require('./es6/components/SortedPricesTable.js');
const Renderer = require('./es6/Renderer.js');

// connect to STOMP and subscribe to FX Prices stream
StompClient.on("connected", () => StompClient.subscribe("/fx/prices", FXPriceReceivedAction));

// create root component
const props = {
  priceType: FXPriceReceivedAction.getType(),
  sortOn: "lastChangeBid"
};
const sortedFXPricesTable = new SortedPricesTable(props);

// render component to DOM
Renderer.render(sortedFXPricesTable, document.getElementById('component-root'));
