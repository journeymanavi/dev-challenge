const Renderer = require('./Renderer.js');
const DUMMY_MARKUP = "<DummyMarkup />";
const stubComponent = {id: "comp_1", componentMounted: () => {}, render: () => { return DUMMY_MARKUP; }};
const stubDOMElement = {};

test("Renderer successfully mounts a component", () => {
  //When
  Renderer.render(stubComponent, stubDOMElement);

  //Then
  expect(Renderer.mountedComponents[stubComponent.id].component).toBe(stubComponent);
});

test("Renderer successfully renders the component right after mounting", () => {
    //When
  Renderer.render(stubComponent, stubDOMElement);

  //Then
  expect(stubDOMElement.innerHTML).toBe(DUMMY_MARKUP);
});

test("Renderer successfully updates a component mounted", () => {
      //When
  Renderer.render(stubComponent, stubDOMElement);
  const updateMarkup = "some other markup";
  stubComponent.render = () => {return updateMarkup};
  Renderer.update(stubComponent.id);

  //Then
  expect(stubDOMElement.innerHTML).toBe(updateMarkup);
});