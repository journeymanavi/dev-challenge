let instance = null;

class Renderer {
  constructor() {
    if(instance === null) {
      this.mountedComponents = {};
      instance = this;
    }
    return instance;
  }

  update(componentId) {
    const mountedComponent = this.mountedComponents[componentId];
    if (mountedComponent) {
      mountedComponent.domElement.innerHTML = mountedComponent.component.render();
    }
  }

  render(component, domElement) {
    this.mountedComponents[component.id] = { component, domElement };
    this.update(component.id);
    component.componentMounted();
  }
}

module.exports = new Renderer;