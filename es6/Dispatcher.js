let instance = null;

class Dispatcher {
  constructor() {
    if(instance === null) {
      instance = this;
      this.actionHandlers = [];
    }
    return instance;
  }

  register(actionHandler) {
    this.actionHandlers.push(actionHandler);
  }

  dispatch(action) {
    this.actionHandlers.forEach(actionHandler => actionHandler(action));
  }
}
module.exports = new Dispatcher;
