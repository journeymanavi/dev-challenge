const EventEmitter = require('events');
const Dispatcher = require('./Dispatcher.js');

const FXPriceReceivedAction = require('./actions/FXPriceReceivedAction.js');
const FX_PRICE_TYPE = FXPriceReceivedAction.getType();

const SUPPORTED_PRICES = [
  FX_PRICE_TYPE
];

const filterOlderMidPrices = (midPrices, time) => {
  return midPrices.filter(m => m.timestamp > (time - 30000));
};

let instance = null;

class Store extends EventEmitter {
  constructor() {
    super();
    if(instance === null) {
      this.prices = {};
      SUPPORTED_PRICES.forEach(priceType => this.prices[priceType] = {});
      Dispatcher.register(this.messageHandler.bind(this));
      instance = this;
    }
    return instance;
  }

  messageHandler(action) {
    const { type, payload: newPrice } = action;
    if(SUPPORTED_PRICES.includes(type)) {
      const now = Date.now();
      const priceName = newPrice.name;

      const oldPrice = this.prices[type][priceName];
      const recentMidPrices = oldPrice !== undefined ? oldPrice.recentMidPrices : [];

      newPrice.recentMidPrices = filterOlderMidPrices(recentMidPrices, now);
      newPrice.recentMidPrices.push({
        timestamp: now,
        midPrice: (newPrice.bestBid + newPrice.bestAsk)/2
      });

      this.prices[type][priceName] = newPrice;
      this.emit("change");
    }
  }

  getPrices(type) {
    if(SUPPORTED_PRICES.includes(type)) {
      return Object.keys(this.prices[type]).map(k => this.prices[type][k]);
    }
    else {
      throw("Unsupported price type: " + type);
    }
  }
}

module.exports = new Store;
