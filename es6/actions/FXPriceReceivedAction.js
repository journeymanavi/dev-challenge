const BaseAction = require('./BaseAction.js');
const type = "RECEIVE_FX_PRICE";
class FXPriceReceivedAction extends BaseAction {
  constructor(payload) {
    super(type, payload);
  }

  static getType() {
    return type;
  }
}
module.exports = FXPriceReceivedAction;