const Dispatcher = require('./Dispatcher.js');
const EventEmitter = require('events');

let client = null;

class StompClient extends EventEmitter {
  constructor() {
    super();
    // single client/connection to handle multiple subscriptions
    if(client === null) {
      client = Stomp.client("ws://localhost:8011/stomp");

      client.debug = function(msg) {
        if (global.DEBUG) {
          console.info(msg)
        }
      };

      client.connect(
        {}, 
        () => {
          document.getElementById('stomp-status').innerHTML = "It has now successfully connected to a stomp server serving price updates for some foreign exchange currency pairs."
          this.emit("connected");
        },
        error => { throw(error) }
      );
    }
  }

  subscribe(destination, Action) {
    client.subscribe(destination, (message) => {
      if(message.body) {
        Dispatcher.dispatch(new Action(JSON.parse(message.body)));
      }
      else {
        console.error("received an empty message body from STOMP");
      }
    });
  }
}
module.exports = new StompClient;
