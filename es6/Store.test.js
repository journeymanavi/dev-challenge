let Dispatcher;

beforeEach(() => {
  jest.mock*('./Dispatcher.js');
  Dispatcher = require('./Dispatcher.js');
  Dispatcher.register = jest.fn();
  Dispatcher.dispatch = jest.fn();
});

afterEach(() => {
  jest.resetModules();
});

test("Store should register with the dispatcher", () => {
  //Given
  let Store = require('./Store.js');

  expect(Dispatcher.register).toHaveBeenCalled();
});

test("Store must successfully store a new incoming message of a supported type", () => {
  //Given
  let Store = require('./Store.js');
  let newPriceName = 'usdeur';
  let newPrice = { name: newPriceName, bestBid: 3.0, bestAsk: 2.0 };
  let FXPriceReceivedAction = require('./actions/FXPriceReceivedAction.js');
  let action = new FXPriceReceivedAction(newPrice);
  let PRICE_TYPE =  FXPriceReceivedAction.getType();

  //When
  Store.messageHandler(action);

  //Then
  expect(Store.prices[PRICE_TYPE][newPriceName]).toBe(newPrice);
});

test("Store must not store a new incoming message of an unsupported type", () => {
  //Given
  let Store = require('./Store.js');
  let UNSUPPORTED_PRICE_TYPE =  "UNSUPPORTED_PRICE_TYPE";
  let Action = require('./actions/BaseAction.js');
  let action = new Action(UNSUPPORTED_PRICE_TYPE, {});

  //When
  Store.messageHandler(action);

  //Then
  expect(Store.prices[UNSUPPORTED_PRICE_TYPE]).not.toBeDefined();
});

test("Store must emit a 'change' event on successfully storing a message", () => {
  //Given
  let Store = require('./Store.js');
  let FXPriceReceivedAction = require('./actions/FXPriceReceivedAction.js');
  let action = new FXPriceReceivedAction({});

  let evendHandler = jest.fn();
  Store.on("change", evendHandler);

  //When
  Store.messageHandler(action);

  //Then
  expect(evendHandler).toHaveBeenCalled();
});

test("Store must successfully return a stored price", () => {
  //Given
  let Store = require('./Store.js');
  let newPriceName = 'usdeur';
  let newPrice = { name: newPriceName, bestBid: 3.0, bestAsk: 2.0 };
  let FXPriceReceivedAction = require('./actions/FXPriceReceivedAction.js');
  let action = new FXPriceReceivedAction(newPrice);
  let PRICE_TYPE =  FXPriceReceivedAction.getType();

  //When
  Store.messageHandler(action);
  let storedPrice = Store.getPrices(PRICE_TYPE);

  //Then
  expect(storedPrice).toEqual([newPrice]);
});