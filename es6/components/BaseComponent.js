const Renderer = require('../Renderer.js');

class BaseComponent {
  constructor(props) {
    this.id = `component_${Date.now()}`;
    this.props = props;
    window[this.id] = this;
  }

  setState(partialState) {
    this.state = Object.assign({}, this.state, partialState);
    Renderer.update(this.id);
  }
}

module.exports = BaseComponent;