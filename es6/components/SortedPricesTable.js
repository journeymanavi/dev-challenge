const BaseComponent = require('./BaseComponent.js');
const Store = require('../Store.js');

class SortedPricesTable extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = { status: "LOADING", sortOn: props.sortOn };
  }

  componentMounted() {
    Store.on("change", this.getPrices.bind(this));
  }

  getPrices() {
    this.setState({
      status: "LOADED",
      prices: Store.getPrices(this.props.priceType)
    });
  }

  render() {
    if(this.state.status === "LOADING") {
      return("Loading..."); 
    } 
    else {
      const sortOn = this.state.sortOn;
      const tableRows = this.state.prices
      .sort((a, b) => {if(a[sortOn]<b[sortOn]) return -1; if(a[sortOn]>b[sortOn]) return 1; return 0;})
      .map(p => `<tr>
          <td style="text-transform:uppercase;">${p.name.slice(0,3)}/${p.name.slice(3)}</td>
          <td>${p.bestBid.toFixed(5)}</td>
          <td>${p.bestAsk.toFixed(5)}</td>
          <td>${p.openBid.toFixed(5)}</td>
          <td>${p.openAsk.toFixed(5)}</td>
          <td>${p.lastChangeBid.toFixed(5)}</td>
          <td>${p.lastChangeAsk.toFixed(5)}</td>
          <td>
            <span id="CCY_${p.name}">
              <img 
                src="blank.png" 
                style="visibility:hidden" 
                onload="Sparkline.draw(document.getElementById('CCY_${p.name}'), 
                [${p.recentMidPrices.map(m => m.midPrice).join(",")}])">
            </span>
          </td>
        </tr>`).join("\n");

      const markup = `<div class="fxPricesTable"><table>
        <tr>
          <th onclick="${this.id}.setState({sortOn: 'name'})">CCY Pair</th>
          <th onclick="${this.id}.setState({sortOn: 'bestBid'})">Best Bid</th>
          <th onclick="${this.id}.setState({sortOn: 'bestAsk'})">Best Ask</th>
          <th onclick="${this.id}.setState({sortOn: 'openBid'})">Open Bid</th>
          <th onclick="${this.id}.setState({sortOn: 'openAsk'})">Open Ask</th>
          <th onclick="${this.id}.setState({sortOn: 'lastChangeBid'})">Last Change Bid</th>
          <th onclick="${this.id}.setState({sortOn: 'lastChangeAsk'})">Last Change Ask</th>
          <th>Recent Mid</th>
        </tr>
        ${tableRows}
      </table></div>`;

      return markup;
    }
  }
}

module.exports = SortedPricesTable;