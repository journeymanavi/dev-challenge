let FXPriceReceivedAction;
let Store;
let SortedPricesTable;
let Renderer;

const [ fxPrice1, fxPrice2, fxPrice3 ] = [
  {
    "name": "eurjpy",
    "bestBid": 12.0,
    "bestAsk": 13.0,
    "openBid": 14.0,
    "openAsk": 15.0,
    "lastChangeBid": 6.8769211401569663,
    "lastChangeAsk": 3.862314256927661
  },
  {
    "name": "usdjpy",
    "bestBid": 2.0,
    "bestAsk": 3.0,
    "openBid": 4.0,
    "openAsk": 5.0,
    "lastChangeBid": -2.8769211401569663,
    "lastChangeAsk": -4.862314256927661
  },
  {
    "name": "usdeur",
    "bestBid": 22.0,
    "bestAsk": 33.0,
    "openBid": 44.0,
    "openAsk": 55.0,
    "lastChangeBid": 2.8769211401569663,
    "lastChangeAsk": 4.862314256927661
  }
];

let fxPriceAction1;
let fxPriceAction2;
let fxPriceAction3;

const ROOT = "component-root";

beforeEach(() => {
  FXPriceReceivedAction = require('../actions/FXPriceReceivedAction.js');
  Store = require('../Store.js');
  SortedPricesTable = require('./SortedPricesTable.js');
  Renderer = require('../Renderer.js');

  fxPriceAction1 = new FXPriceReceivedAction(fxPrice1);
  fxPriceAction2 = new FXPriceReceivedAction(fxPrice2);
  fxPriceAction3 = new FXPriceReceivedAction(fxPrice3);

  document.body.innerHTML = `<div id="${ROOT}"></div>`
});

afterEach(() => {
  jest.resetModules();
});

test("SortedPricesTable component should get successfully initialised with props and initial state", () => {
  //Given
  const props = {
    priceType: FXPriceReceivedAction.getType(),
    sortOn: "lastChangeBid"
  };

  //When
  const sortedFXPricesTable = new SortedPricesTable(props);

  //Then
  expect(sortedFXPricesTable.props).toBe(props);
  expect(sortedFXPricesTable.state).toEqual({status: "LOADING", sortOn: "lastChangeBid"});
});

test("SortedPricesTable component's render method should return initial loading state correctly", () => {
  //Given
  const props = {
    priceType: FXPriceReceivedAction.getType(),
    sortOn: "lastChangeBid"
  };
  const sortedFXPricesTable = new SortedPricesTable(props);

  //When
  Renderer.render(sortedFXPricesTable, document.getElementById(ROOT));

  //Then
  expect(document.getElementById(ROOT).innerHTML).toEqual("Loading...");
});

test("SortedPricesTable component's render method should update the sorted view as and when new data is received", () => {
  //Given
  const props = {
    priceType: FXPriceReceivedAction.getType(),
    sortOn: "lastChangeBid"
  };
  const sortedFXPricesTable = new SortedPricesTable(props);
  Renderer.render(sortedFXPricesTable, document.getElementById(ROOT));

  //When
  Store.messageHandler(fxPriceAction1);
  //Then
  let priceTable = document.querySelector("div.fxPricesTable > table");
  expect(priceTable).toBeDefined();
  expect(priceTable.rows.length).toEqual(2);
  expect(priceTable.rows[1].cells[0].innerHTML).toEqual("eur/jpy");

  //When
  Store.messageHandler(fxPriceAction2);
  //Then
  priceTable = document.querySelector("div.fxPricesTable > table");
  expect(priceTable.rows.length).toEqual(3);
  expect(priceTable.rows[1].cells[0].innerHTML).toEqual("usd/jpy");
  expect(priceTable.rows[2].cells[0].innerHTML).toEqual("eur/jpy");
  


  //When
  Store.messageHandler(fxPriceAction3);
  //Then
  priceTable = document.querySelector("div.fxPricesTable > table");
  expect(priceTable.rows.length).toEqual(4);
  expect(priceTable.rows[1].cells[0].innerHTML).toEqual("usd/jpy");
  expect(priceTable.rows[2].cells[0].innerHTML).toEqual("usd/eur");
  expect(priceTable.rows[3].cells[0].innerHTML).toEqual("eur/jpy");
});

test("SortedPricesTable component's render method should return the right (sorted) markup", () => {
  //Given
  const props = {
    priceType: FXPriceReceivedAction.getType(),
    sortOn: "lastChangeBid"
  };
  const sortedFXPricesTable = new SortedPricesTable(props);

  //When
  Renderer.render(sortedFXPricesTable, document.getElementById(ROOT));
  Store.messageHandler(fxPriceAction1);
  Store.messageHandler(fxPriceAction2);
  Store.messageHandler(fxPriceAction3);
  
  //Then
  const priceTable = document.querySelector("div.fxPricesTable > table");
  expect(priceTable).toBeDefined();
  expect(priceTable.rows.length).toEqual(4);
  expect(priceTable.rows[1].cells[0].innerHTML).toEqual("usd/jpy");
  expect(priceTable.rows[2].cells[0].innerHTML).toEqual("usd/eur");
  expect(priceTable.rows[3].cells[0].innerHTML).toEqual("eur/jpy");
});