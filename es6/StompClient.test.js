test("StompClient should successfully connect and accept subscription for STOMP destination", (done) => {
  //Given
  document.body.innerHTML = `
  <div id="stomp-status" />
  `;

  jest.mock('./Dispatcher.js');
  const Dispatcher = require('./Dispatcher.js');
  Dispatcher.dispatch = jest.fn();
  const mockAction = jest.fn();
  const dummyMessage = '{"dummy":"message"}';

  const stubbedClient = {
    connect: jest.fn((obj, scb, ecb) => {scb();}),
    subscribe: jest.fn((destination, cb) => { cb({"body": dummyMessage}); })
  };

  global.Stomp = {
    client: function() { return stubbedClient }
  };

  //When
  const StompClient = require('./StompClient.js');
  StompClient.subscribe("dummyDestination", mockAction);

  //Then
  expect(stubbedClient.connect).toHaveBeenCalled();
  expect(document.getElementById("stomp-status").innerHTML).toEqual("It has now successfully connected to a stomp server serving price updates for some foreign exchange currency pairs.");
  expect(Dispatcher.dispatch).toHaveBeenCalled();
  expect(mockAction).toHaveBeenCalledWith(JSON.parse(dummyMessage));
  done();
});