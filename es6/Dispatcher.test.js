test("Dispatcher must be a singleton", () => {
  //Given
  const instance1 = require('./Dispatcher.js');

  //When
  const instance2 = require('./Dispatcher.js');

  //THen
  expect(instance2).toBe(instance1);
});

const setupDispatcher = () => {
  return {
    DUMMY_ACTION: "DummyAction",
    actionHandler1: jest.fn(),
    actionHandler2: jest.fn(),
    Dispatcher: require('./Dispatcher.js')
  }
};

test("Dispatcher must register action handlers", () => {
  //Given
  const { DUMMY_ACTION, actionHandler1, actionHandler2, Dispatcher } = setupDispatcher();

  //WHen
  Dispatcher.register(actionHandler1);
  Dispatcher.register(actionHandler2);

  //Then
  expect(Dispatcher.actionHandlers).toContain(actionHandler1);
  expect(Dispatcher.actionHandlers).toContain(actionHandler2);
});

test("Dispatcher must call action handlers", () => {
  //Given
  const { DUMMY_ACTION, actionHandler1, actionHandler2, Dispatcher } = setupDispatcher();
  Dispatcher.register(actionHandler1);
  Dispatcher.register(actionHandler2);

  //When
  Dispatcher.dispatch(DUMMY_ACTION);

  //Then
  expect(actionHandler1).toHaveBeenCalledWith(DUMMY_ACTION);
  expect(actionHandler2).toHaveBeenCalledWith(DUMMY_ACTION);
});